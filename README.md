# 介绍

仿12306系统后台，基于JDK17和SpringBoot3

# 项目结构

MVC三层架构

# 技术选型：

## 后端

- 基础：Java17，Spring Boot3，Mybaits

- 项目构建：Maven3.6以上
- 认证：JWT
- 日志处理：Logback
- 接口规范：Restful风格
- 关系型数据库：MySQL5/MySQL8
- 数据库连接池：Druid
- 缓存：Redis
- 定时任务：Quartz
- 短信：腾讯云短信服务
- 负载均衡+web服务器：Nginx
- 网关：Gateway
- 服务调用：openFeign
- 其他：fastjson、hutool工具包
## 前端

- 框架：AntDesignVue
- 构建工具：webpack
- JS版本：ES6
- 基础JS框架：Vue.js，Node.js
- 路由管理：Vue Router
- 状态管理：Vuex
- 基础UI库 & UI界面：iView
- 网络请求：Axios

