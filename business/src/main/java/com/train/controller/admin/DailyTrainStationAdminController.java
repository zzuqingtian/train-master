package com.train.controller.admin;

import com.train.req.DailyTrainStationQueryReq;
import com.train.req.DailyTrainStationSaveReq;
import com.train.resp.CommonResp;
import com.train.resp.DailyTrainStationQueryResp;
import com.train.resp.PageResp;
import com.train.service.DailyTrainStationService;
import jakarta.annotation.Resource;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.*;

/**
 * 每日火车车站管理
 */
@RestController
@RequestMapping("/admin/daily-train-station")
public class DailyTrainStationAdminController {


    @Resource
    private DailyTrainStationService dailyTrainStationService;

    @PostMapping("/save")
    public CommonResp<Object> save(@Valid @RequestBody DailyTrainStationSaveReq req){
        dailyTrainStationService.save(req);
        return new CommonResp<>();
    }

    @GetMapping("/query-list")
    public CommonResp<PageResp<DailyTrainStationQueryResp>> queryList(@Valid DailyTrainStationQueryReq req) {
        PageResp<DailyTrainStationQueryResp> list = dailyTrainStationService.queryList(req);
        return new CommonResp<>(list);
    }

    @DeleteMapping("/delete/{id}")
    public CommonResp<Object> delete(@PathVariable Long id) {
        dailyTrainStationService.delete(id);
        return new CommonResp<>();
    }
}
