package com.train.controller.admin;

import com.train.req.TrainSeatQueryReq;
import com.train.req.TrainSeatSaveReq;
import com.train.req.TrainStationSaveReq;
import com.train.resp.CommonResp;
import com.train.resp.PageResp;
import com.train.resp.TrainSeatQueryResp;
import com.train.service.DailyTrainSeatService;
import com.train.service.TrainSeatService;
import com.train.service.TrainStationService;
import jakarta.annotation.Resource;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.*;

/**
 * 火车座位管理
 */
@RestController
@RequestMapping("/admin/train-seat")
public class TrainSeatAdminController {

    @Resource
    private TrainSeatService trainSeatService;

    @PostMapping("/save")
    public CommonResp<Object> save(@Valid @RequestBody TrainSeatSaveReq req) {
        trainSeatService.save(req);
        return new CommonResp<>();
    }

    @GetMapping("/query-list")
    public CommonResp<PageResp<TrainSeatQueryResp>> queryList(@Valid TrainSeatQueryReq req){
        PageResp<TrainSeatQueryResp> list = trainSeatService.queryList(req);
        return new CommonResp<>(list);
    }

    @DeleteMapping("/delete/{id}")
    public CommonResp<Object> delete(@PathVariable Long id) {
        trainSeatService.delete(id);
        return new CommonResp<>();
    }
}
