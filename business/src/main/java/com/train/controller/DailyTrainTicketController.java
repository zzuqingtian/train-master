package com.train.controller;

import com.train.req.DailyTrainTicketQueryReq;
import com.train.resp.CommonResp;
import com.train.resp.DailyTrainTicketQueryResp;
import com.train.resp.PageResp;
import com.train.service.DailyTrainTicketService;
import jakarta.annotation.Resource;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Auther: 赵羽
 * @Date: 2023/6/13 - 06 - 13 - 15:53
 * @Description: 每日车票管理
 * @version: 1.0
 */
@RestController
@RequestMapping("/daily-train-ticket")
public class DailyTrainTicketController {

    @Resource
    private DailyTrainTicketService dailyTrainTicketService;

    @GetMapping("/query-list")
    public CommonResp<PageResp<DailyTrainTicketQueryResp>> queryList(@Valid DailyTrainTicketQueryReq req) {
        PageResp<DailyTrainTicketQueryResp> list = dailyTrainTicketService.queryList(req);
        return new CommonResp<>(list);
    }
}
