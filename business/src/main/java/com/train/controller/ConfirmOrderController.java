package com.train.controller;

import com.train.req.ConfirmOrderDoReq;
import com.train.resp.CommonResp;
import com.train.service.ConfirmOrderService;
import jakarta.annotation.Resource;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Auther: 赵羽
 * @Date: 2023/6/13 - 06 - 13 - 15:25
 * @Description: 确认订单管理
 * @version: 1.0
 */
@RestController
@RequestMapping("/confirm-order")
public class ConfirmOrderController {


    @Resource
    private ConfirmOrderService confirmOrderService;

    @PostMapping("/do")
    public CommonResp<Object> doConfirm(@Valid @RequestBody ConfirmOrderDoReq req) {
        confirmOrderService.doConfirm(req);
        return new CommonResp<>();
    }
}
