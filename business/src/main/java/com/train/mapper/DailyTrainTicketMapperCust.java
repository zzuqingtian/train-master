package com.train.mapper;

import org.apache.ibatis.annotations.Mapper;

import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author 老顾
 * @since 2023-06-16
 */
@Mapper
public interface DailyTrainTicketMapperCust {

    void updateCountBySell(Date date, String trainCode, String seatType, Integer minStartIndex, Integer maxStartIndex, Integer minEndIndex, Integer maxEndIndex);

}
