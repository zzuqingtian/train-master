package com.train.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *
 * </p>
 *
 * @author 老顾
 * @since 2023-06-11
 */
@RestController
public class HelloController {


    @GetMapping("hello")
    public void hello(){
        System.out.println("HelloWorld!!");
    }
}
