package com.train.mapper;

import com.train.domain.Passenger;

/**
 * <p>
 *
 * </p>
 *
 * @author 老顾
 * @since 2023-06-16
 */
public interface PassengerAllMapper {
    Passenger selectByMemberId(Long passengerId);

}
